#include <assert.h>
#include <map>
#include <limits>

template<class _Key, class V>
class interval_map 
{
	template <typename K, typename V, std::size_t C>
    friend void IntervalMapTest_params();

private:
    std::map<_Key,V> m_map;

public:
    // constructor associates whole range of K with val by inserting (K_min, val)
    // into the map
    interval_map (V const& val) 
	{
        m_map.insert (m_map.begin(), std::make_pair (std::numeric_limits<_Key>::lowest(), val));
    };

    // Assign value val to interval [keyBegin, keyEnd). 
    // Overwrite previous values in this interval. 
    // Do not change values outside this interval.
    // Conforming to the C++ Standard Library conventions, the interval 
    // includes keyBegin, but excludes keyEnd.
    // If !( keyBegin < keyEnd ), this designates an empty interval, 
    // and assign must do nothing.
    void assign (_Key const& keyBegin, _Key const& keyEnd, const V& val) 
	{
// INSERT YOUR SOLUTION HERE
		if (!(keyBegin < keyEnd))
		{
			return;
		}
		
		auto _end_of_range = m_map.upper_bound(keyEnd);

		const auto _prev_val = std::prev(_end_of_range)->second;

		_end_of_range = m_map.emplace_hint(_end_of_range, keyEnd, _prev_val);
		_end_of_range->second = _prev_val;

		auto _beg_of_range = m_map.upper_bound(keyBegin);
		_beg_of_range = m_map.emplace_hint(_beg_of_range, keyBegin, val);
		_beg_of_range->second = val;


		while (_beg_of_range != m_map.begin()
			&& std::prev(_beg_of_range)->second == _beg_of_range->second)
			--_beg_of_range;

		while (_end_of_range != m_map.end() 
			&& (   std::next(_end_of_range) != m_map.end()
			    && _end_of_range->second == std::next(_end_of_range)->second
			    || _end_of_range->second == _beg_of_range->second))
			++_end_of_range;
		
		m_map.erase(std::next(_beg_of_range), _end_of_range);		

    }

    // look-up of the value associated with key
    V const& operator[] (_Key const& key) const 
	{
        return (--m_map.upper_bound (key))->second;
    }
};

// Many solutions we receive are incorrect. Consider using a randomized test
// to discover the cases that your implementation does not handle correctly.
// We recommend to implement a function IntervalMapTest() here that tests the
// functionality of the interval_map, for example using a map of unsigned int
// intervals to char.

#include "tdd_suite.hpp"
#include "restrict_value.hpp"
#include <iomanip>

template <typename _Key, typename _Val, std::size_t _Niter>
void IntervalMapTest_params()
{
	typedef _Key key_type;
	typedef _Val val_type;

	const auto _initialValue = random_value_in_full_range<val_type>();

	interval_verification<key_type, val_type> theRef(_initialValue);
	interval_map<key_type, val_type> theMap(_initialValue);

	theRef.full_sweep<val_type>(theMap);

	std::cout << "Initial value interval "
		<< std::make_pair(std::array<int, 2>
		{
			std::numeric_limits<key_type>::lowest(),
				std::numeric_limits<key_type>::max() + 1
		},
		_initialValue)
		<< "\n";
	for (auto i = 0ull; i < _Niter; ++i)
	{
		auto v = make_random_interval<key_type, val_type>();

		std::cout << "Testing " << v << "\n";

		theRef.assign(v.first[0], v.first[1], v.second);
		theMap.assign(v.first[0], v.first[1], v.second);

		check_cannonicality(theMap.m_map);

		theRef.full_sweep<val_type>(theMap);
	}

}

void IntervalMapTest()
{
	{
		// Test if compiles under restricted conditions
		interval_map<restrict_key<int>, restrict_value<char>> v('a');
		v.assign(1, 5, 'b');
		MyAssert(v[3] == 'b');
		MyAssert(v[7] == 'a');
	}

	IntervalMapTest_params<std:: int8_t, std::uint8_t, 10000>();
	IntervalMapTest_params<std::uint8_t, std::uint8_t, 10000>();
	IntervalMapTest_params<std:: int8_t,  std::int8_t, 10000>();
	IntervalMapTest_params<std::uint8_t,  std::int8_t, 10000>();

	IntervalMapTest_params<std:: int16_t, std::uint16_t, 10000>();
	IntervalMapTest_params<std::uint16_t, std::uint16_t, 10000>();
	IntervalMapTest_params<std:: int16_t,  std::int16_t, 10000>();
	IntervalMapTest_params<std::uint16_t,  std::int16_t, 10000>();
}


int main(int argc, char* argv[]) try {
    IntervalMapTest();
    return 0;
}
catch (const std::exception& e)
{
	std::cout << e.what() << "\n";
	return 0;
}
