#pragma once

template <typename T>
struct restrict_key
{
	auto&& value() const { return m_value; }

	friend auto operator < (const restrict_key<T>& a, const restrict_key<T>& b)
	{
		return a.value() < b.value();
	}

	restrict_key(const T& _init) :
		m_value(_init)
	{}

	restrict_key():
		restrict_key(T ())
	{}

	restrict_key(const restrict_key<T>& a) :
		restrict_key(a.value ())
	{}

	restrict_key<T>& operator = (const restrict_key<T>& a)
	{		
		new(this) restrict_key<T>(a);
		return *this;
	}

private:
	T m_value;
};


template <typename T>
struct restrict_value
{
	auto&& value() const { return m_value; }

	friend auto operator == (const restrict_value<T>& a, const restrict_value<T>& b)
	{
		return a.value() == b.value();
	}

	restrict_value(const T& _init) :
		m_value(_init)
	{}

	restrict_value() :
		restrict_value(T())
	{}

	restrict_value(const restrict_value<T>& a) :
		restrict_value(a.value())
	{}

	restrict_value<T>& operator = (const restrict_value<T>& a)
	{
		new(this) restrict_value<T>(a);
		return *this;
	}

private:
	T m_value;
};


#include <limits>

namespace std 
{
	template <typename T>
	struct numeric_limits<restrict_key<T>> : numeric_limits<T> {};
	template <typename T>
	struct numeric_limits<restrict_value<T>> : numeric_limits<T> {};
}

