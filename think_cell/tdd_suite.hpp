#pragma once

#include <iostream>
#include <cassert>
#include <random>
#include <array>
#include <type_traits>
#include <memory>
#include <algorithm>
#include <string>
#include <iomanip>
#include <stdexcept>

#define MyAssert(X) if(!(X)) throw std::runtime_error("Failed assertion : " #X);

template <typename _Key, typename V>
void check_cannonicality(const std::map<_Key, V>& theMap)
{
	const std::pair<const _Key, V>* lastItem = nullptr;
	for (const auto& thePair : theMap)
	{
		if (lastItem)
		{
			MyAssert(lastItem->second != thePair.second);
		}
		lastItem = &thePair;
	}
}

auto& mt_generator()
{
	static std::random_device rndDev;
	static std::mt19937_64 mtRand(rndDev());
	return mtRand;
}

template <typename _Value>
auto random_value_in_full_range()
{
	static std::uniform_int<_Value> vDist(
		std::numeric_limits<_Value>::lowest(),
		std::numeric_limits<_Value>::max()
	);
	return vDist(mt_generator ());
}

template <typename _Value>
auto random_value_in_range(const _Value& a, const _Value& b)
{
	std::uniform_int<_Value> vDist (a, b);
	return vDist(mt_generator());
}

template <typename _Key, typename V>
struct interval_verification
{
	static constexpr std::intmax_t  _Interval_max = std::numeric_limits<_Key>::max();
	static constexpr std::intmax_t  _Interval_min = std::numeric_limits<_Key>::lowest();
	static constexpr std::uintmax_t _Interval_size = _Interval_max - _Interval_min + 1;

	std::array<V, _Interval_size> m_interval_mask;

	interval_verification(const V& _init)
	{
		std::fill(m_interval_mask.begin(), m_interval_mask.end(), _init);
	}

	void assign(const _Key& keyBegin, const _Key& keyEnd, const V& value)
	{
		for (std::intmax_t i = keyBegin; i < keyEnd; ++i)
		{
			m_interval_mask[std::intmax_t(i) - _Interval_min] = value;
		}
	}

	const V& operator[] (const _Key& index) const
	{
		return m_interval_mask[std::intmax_t(index) - _Interval_min];
	}

	template <typename _Key, typename _IMap>
	void full_sweep(const _IMap& iMap) const
	{
		for (std::intmax_t i = _Interval_min; i <= _Interval_max; ++i)
		{
			MyAssert(iMap[_Key(i)] == (*this)[_Key(i)]);				
		}
	}

	template <typename _Key, typename _IMap>
	void random_sweep(const _IMap& iMap, std::size_t nTimes) const
	{
		for (auto n = nTimes; n > 0; --n)
		{
			const auto i = random_value_in_full_range<_Key>();
			MyAssert(iMap[i] == (*this)[i]);
		}
	}

};

template <typename _Key, typename _Value>
auto make_random_interval()
{

	const auto k0 = random_value_in_full_range<_Key>();
	const auto k1 = random_value_in_full_range<_Key>();
	const auto v0 = random_value_in_full_range<_Value>();

	return std::make_pair(std::array<_Key, 2>{std::min(k0, k1), std::max(k0, k1)}, v0);
}

template <typename _Key, typename _Value>
std::ostream& operator << (std::ostream& oss, const std::pair<std::array<_Key, 2>, _Value>& thePair)
{
	return oss << "{[" << +thePair.first [0] << ", " << +thePair.first [1] <<  "): " << +thePair.second << "}";
}
